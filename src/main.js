require('dotenv').config();

let mc = require('minecraft-protocol');
let fs = require('fs');

let faviconBase64 = undefined;
try { 
	let faviconData = fs.readFileSync('server-icon.png');
	faviconBase64 = 'data:image/png;base64,' + faviconData.toString('base64');
} catch (error) {
	faviconBase64 = undefined;
}

let server = mc.createServer({
	'online-mode': true,
	port: process.env.PLACEHOLDER_PORT || 25565,
	version: process.env.PLACEHOLDER_VERSION || '1.8.8',
	motd: process.env.PLACEHOLDER_MOTD || 'Hello from Placeholder!',
	favicon: faviconBase64
});

let mcData = require('minecraft-data')(server.version);
let loginPacket = mcData.loginPacket;

server.on('login', (client) => {
	console.log('Incoming connection from ' + client.socket.remoteAddress);
	client.end(process.env.PLACEHOLDER_KICK_MESSAGE || 'Goodbye from Placeholder!');
});

server.on('error', (error) => {
	console.error(error);
});

server.on('listening', () => {
	console.log('Server listening on port ' + server.socketServer.address().port);
});
