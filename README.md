# Placeholder

Placeholder uses `node-minecraft-protocol` to pretend to be a Minecraft server.

The intended use is to be a placeholder for the actual server, when the server is offline. 

This was written for Raimu, a Minecraft Reddit-UHC server. Since matches are scheduled, the server doesn't actually need to be online if we aren't currently hosting a match. So, instead of actually keeping it up, we shut it down and start up Placeholder.

## Usage

1. `cp .env.example .env`
2. Fill out `.env`
3. Put a 64x64 PNG as `server-icon.png` for the server icon
4. `yarn install`
5. `yarn start`
